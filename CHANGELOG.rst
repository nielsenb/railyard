Changelog
=========

railyard 0.1.1-dev.0
====================

*Release date: YYYY-MM-DD*

Added
-----
* `pyproject.toml` file

Changed
-------
* Following `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_ for this and future CHANGELOG entries
* Update black to 21.9b0
* Change black language_version to python3

railyard 0.1.0
==============

*Release date: 2020-01-02*

Changes
-------

* HTTP errors, including redirects, are now proxied with no modification
* The first handler with :code:`static_files` or :code:`static_dir` now takes priority over any other matching handler to match AppEngine behavior
* :code:`CONTENT_TYPE` and :code:`CONTENT_LENGTH` headers are now proxied
* Add abililty to override entyrpoints from the command line using :code:`-e` / :code:`--entrypoint`

railyard 0.0.4
==============

*Release date: 2019-11-20*

Changes
-------

* Change to default Sphinx theme
* Add documentation link

railyard 0.0.3
==============

*Release date: 2019-11-20*

Changes
-------

* More docs fixes

railyard 0.0.2
==============

*Release date: 2019-11-20*

Changes
-------

* Docs fixes

railyard 0.0.1
==============

*Release date: 2019-11-20*

Changes
-------

* Initial release
